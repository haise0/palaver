// ---------------------------------------------------------------------------------------------------------------------
// Palaver Main Application
// ---------------------------------------------------------------------------------------------------------------------

import { app } from 'electron';

// Managers
import { createWindow, getAllWindows } from './managers/window';

// Utils
import ipcUtil from '../common/ipc';

// Contexts
import { context as testContext } from './contexts/test';

// ---------------------------------------------------------------------------------------------------------------------

function createMainWindow() : void
{
    createWindow('main', 'app://-', {
        height: 600,
        width: 800,
        webPreferences: {
            contextIsolation: true
        }
    });
} // end createMainWindow

// ---------------------------------------------------------------------------------------------------------------------

app.setAboutPanelOptions({
    applicationName: 'Palaver',
    copyright: 'Skewed Aspect Studios',
    authors: [ 'Christopher S. Case <chris.case@g33xnexus.com>' ],
    iconPath: './static/logo/logo_trans.png'
});

// ---------------------------------------------------------------------------------------------------------------------

app.on('ready', () =>
{
    // Register IPC context handlers
    ipcUtil.registerContext('test', testContext);

    // -----------------------------------------------------------------------------------------------------------------

    createMainWindow();

    app.on('activate', () =>
    {
        const windows = Object.values(getAllWindows());
        if(windows.length === 0)
        {
            // On macOS it's common to re-create a window in the app when the dock icon is clicked and there are no
            // other windows open.
            createMainWindow();
        } // end if
    });

    // Quit when all windows are closed, except on macOS. There, it's common for applications and their menu bar to
    // stay active until the user quits explicitly with Cmd + Q.
    app.on('window-all-closed', () =>
    {
        if(process.platform !== 'darwin')
        {
            app.quit();
        } // end if
    });
});

// ---------------------------------------------------------------------------------------------------------------------
