// ---------------------------------------------------------------------------------------------------------------------
// WindowManager
// ---------------------------------------------------------------------------------------------------------------------

import { resolve } from 'path';

import { BrowserWindow, BrowserWindowConstructorOptions } from 'electron';
import serve from 'electron-serve';

// ---------------------------------------------------------------------------------------------------------------------

const windows = new Map<string, BrowserWindow>();
serve({ directory: resolve(__dirname, '..', '..', 'renderer') });

// ---------------------------------------------------------------------------------------------------------------------

export async function createWindow(name : string, url : string, options : BrowserWindowConstructorOptions, showDebug = false) : Promise<BrowserWindow>
{
    const win = new BrowserWindow(options);
    await win.loadURL(url);

    if(showDebug)
    {
        win.webContents.openDevTools();
    } // end if

    win.on('closed', () => { windows.delete(name); });

    windows.set(name, win);
    return win;
} // end createWindow

export function getWindow(name : string) : BrowserWindow | undefined
{
    return windows.get(name);
} // end getWindow

export function getAllWindows() : Record<string, BrowserWindow>
{
    const obj : Record<string, BrowserWindow> = {};
    windows.forEach((win, name) => obj[name] = win);
    Object.freeze(obj);

    return obj;
} // end getAllWindows

// ---------------------------------------------------------------------------------------------------------------------
