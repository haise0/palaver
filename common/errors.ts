// ---------------------------------------------------------------------------------------------------------------------
// Custom Errors
// ---------------------------------------------------------------------------------------------------------------------

export class AppError extends Error
{
    public code = 'APP_ERROR';

    constructor(message : string)
    {
        super(message);
    } // end constructor

    public toJSON() : { name : string, message : string, code : string, stack ?: string }
    {
        return {
            name: this.constructor.name,
            message: this.message,
            code: this.code,
            stack: this.stack
        };
    } // end toJSON
} // end ServiceError

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownContextError extends AppError
{
    public code = 'UNKNOWN_CONTEXT_ERROR';

    constructor(context ?: string)
    {
        super(`Missing or unrecognized context${ context ? ` '${ context }'` : '' }.`);
    } // end constructor
} // end UnknownContextError

// ---------------------------------------------------------------------------------------------------------------------

export class UnknownOperationError extends AppError
{
    public code = 'UNKNOWN_OPERATION_ERROR';

    constructor(operation ?: string)
    {
        super(`Missing or unrecognized operation${ operation ? ` '${ operation }'` : '' }.`);
    } // end constructor
} // end UnknownOperationError

// ---------------------------------------------------------------------------------------------------------------------
