//----------------------------------------------------------------------------------------------------------------------
// IpcMainClient
//----------------------------------------------------------------------------------------------------------------------

import { ipcMain, ipcRenderer, IpcMainInvokeEvent } from 'electron';

import { IpcContext } from './context';
import { UnknownContextError } from '../errors';

//----------------------------------------------------------------------------------------------------------------------

export interface IpcRequest {
    context : string;
    operation : string;
    payload : unknown;
}

//----------------------------------------------------------------------------------------------------------------------

export class IpcUtil
{
    readonly #contexts : Map<string, IpcContext> = new Map();

    //------------------------------------------------------------------------------------------------------------------

    constructor()
    {
        ipcMain.handle('ipc-request', this.$handleRequest.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------

    protected async $handleRequest(_event : IpcMainInvokeEvent, request : IpcRequest) : Promise<unknown>
    {
        const context = this.#contexts.get(request.context);
        if(!context)
        {
            throw new UnknownContextError(request.context);
        } // end if

        return context.$callOperation(request.operation, request.payload);
    } // end $handleRequest

    //------------------------------------------------------------------------------------------------------------------

    public registerContext(name : string, context : IpcContext) : void
    {
        this.#contexts.set(name, context);
    } // end registerContext

    public async request<T>(context : string, operation : string, payload : unknown) : Promise<T>
    {
        return ipcRenderer.invoke('ipc-request', {
            context,
            operation,
            payload
        });
    } // end request
} // end IpcClient

//----------------------------------------------------------------------------------------------------------------------
