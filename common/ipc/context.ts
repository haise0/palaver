//----------------------------------------------------------------------------------------------------------------------
// IpcContext
//----------------------------------------------------------------------------------------------------------------------

import { UnknownOperationError } from '../errors';

//----------------------------------------------------------------------------------------------------------------------

export type OperationFunction<T> = (payload : T) => Promise<unknown>

//----------------------------------------------------------------------------------------------------------------------

export class IpcContext
{
    readonly #operations : Map<string, OperationFunction<unknown>> = new Map();

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Internal function to call the operation and return the results.
     *
     * @internal
     *
     * @param opName - The name of the operation to call.
     * @param payload - Any data received as part of the request.
     *
     * @returns Returns anything that the operation function resolves to.
     */
    public async $callOperation(opName : string, payload : unknown) : Promise<unknown>
    {
        const opFunc = this.#operations.get(opName);
        if(!opFunc)
        {
            throw new UnknownOperationError(opName);
        } // end if

        return opFunc(payload);
    } // end $callOperation

    //------------------------------------------------------------------------------------------------------------------

    /**
     * Declares a function to be an exposed operation. The function will be called with whatever data was passed in to
     * the request, and is expected to return a response, if any is expected on the other side.
     *
     * @param opName - The name we want the function to be exposed under.
     * @param opFunc - The actual function to expose.
     */
    public operation<T>(opName : string, opFunc : OperationFunction<T>) : void
    {
        this.#operations.set(opName, opFunc as OperationFunction<unknown>);
    } // end operation
} // end IpcContext

//----------------------------------------------------------------------------------------------------------------------
